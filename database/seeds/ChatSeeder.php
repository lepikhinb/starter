<?php

use Illuminate\Database\Seeder;

class ChatSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();
        $user = \App\Models\User::oldest()->first();

        $count = rand(2, 5);

        $ignored = [$user->id];

        for($i = 0; $i < $count; $i++)
        {
            $user2 = \App\Models\User::whereNotIn('id', $ignored)->inRandomOrder()->first();

            $ignored[] = $user2->id;

            $conversation = $user->ownConversations()->create();
            $conversation->users()->sync([$user->id, $user2->id]);

            $messages = rand(1, 3);

            for($k = 0; $k < $messages; $k++)
            {
                $conversation->messages()->create([
                    'body'      => $faker->sentence,
                    'user_id'   => $conversation->users()->inRandomOrder()->first()->id,
                ]);
            }
        }

    }
}
