<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // superadmin
        $user = User::create([
            'first_name'    => 'Super',
            'last_name'     => 'Admin',
            'email'         => 'superadmin@test.com',
            'phone'         => $faker->e164PhoneNumber,
            'password'      => bcrypt('secret'),
            'image'         => 'https://api.adorable.io/avatars/285/1.png',
        ]);

        $user->profile()->save(factory(App\Models\Profile::class)->make());

        factory(App\Models\User::class, 50)->create()->each(function ($user) {
            $user->profile()->save(factory(App\Models\Profile::class)->make());
        });

        $users = User::all();

        foreach($users as $user)
        {
            $user->update(['image' => 'https://api.adorable.io/avatars/285/' . $user->id . '.png']);

            // friends
            $friendsCount = rand(10, 25);
            $friends = \App\Models\User::inRandomOrder()->where('id', '<>', $user->id)->limit($friendsCount)->get();
            foreach($friends as $friend)
            {
                $friend->befriend($user);
            }
        }
    }
}