<?php


/**
 * Стандартный роут для SPA-приложения
 */

Route::get('{path}', function () {
    return view('index');
})->where('path', '(.*)');
