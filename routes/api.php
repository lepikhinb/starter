<?php

Route::middleware('auth:api')->namespace('Api')->group(function () {

    /**
     * Пользователи
     */
    Route::apiResource('users', 'UserController');

    /**
     * Уведомления – все; непрочитанные; прочитать уведомление
     */
    Route::get('notifications', 'NotificationController@index')->name('notifications.index');
    Route::get('notifications/unread', 'NotificationController@unread')->name('notifications.unread');
    Route::post('notifications/{id}', 'NotificationController@read')->name('notifications.read');

    /**
     * Поставить лайк на сущности
     */
    Route::post('likes/{model}/{id}', 'LikeController@toggle')->name('likes.toggle');

    /**
     * Комментарии – добавление; изменение; удаление
     */
    Route::post('comments/{model}/{modelId}', 'CommentController@store')->name('comments.store');
    Route::resource('comments', 'CommentController')->only(['update', 'destroy']);

    /**
     * Аттачменты – загрузка; удаление
     */
    Route::delete('attachments/{id}', 'AttachmentController@remove')->name('attachments.remove');
    Route::post('attachments/{model}', 'AttachmentController@upload')->name('attachments.upload');

    /**
     * Чат – начать диалог; получить переписки; получить сообщения; отправить сообщение; удалить сообщение;
     */
    Route::post('users/{id}/chat', 'ChatController@start');
    Route::get('conversations', 'ChatController@getConversations');
    Route::post('conversations/{conversation}/messages', 'ChatController@sendMessage');
    Route::delete('conversations/{conversation}/messages/{message}', 'ChatController@removeMessage');
    Route::put('conversations/{conversation}/messages/{message}', 'ChatController@updateMessage');
    Route::resource('conversations', 'ChatController')->only(['show']);

    /**
     * Друзья – друзья; подписчики; подписки; добавление и удаление друзей
     */
    Route::get('followers', 'FriendController@followers')->name('followers.index');
    Route::get('following', 'FriendController@following')->name('following.index');
    Route::get('friends', 'FriendController@friends')->name('friends.index');
    Route::post('friends/{user}', 'FriendController@befriend')->name('friends.befriend');
    Route::delete('friends/{user}', 'FriendController@unfriend')->name('friends.unfriend');
});

/**
 * Стандартные роуты для JWT-авторизации
 */
Route::middleware('api')->prefix('auth')->namespace('Api')->group(function () {
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('refresh', 'AuthController@refresh')->name('refresh');
    Route::get('me', 'AuthController@me')->name('me');
});
