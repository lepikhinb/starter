<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Http\Request;
use App\Http\Resources\NotificationResource;

class NotificationController extends Controller
{
    private $user;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $notifications = $this->user
            ->notifications()
            ->whereNotNull('read_at')
            ->latest()
            ->paginate(10);

        return NotificationResource::collection($notifications);
    }

    public function unread(Request $request)
    {
        $notifications = $this->user
            ->unreadNotifications()
            ->latest()
            ->paginate(10);

        return NotificationResource::collection($notifications);
    }

    public function read($notificationId)
    {
        $notification = DatabaseNotification::findOrFail($notificationId);
        $notification->update(['read_at' => now()]);

        return response()->json([
            'success' => true,
        ]);
    }
}
