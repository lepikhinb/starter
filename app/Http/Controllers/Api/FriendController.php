<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\RelatedUserResource;

class FriendController extends Controller
{
    private $user;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function followers(Request $request)
    {
        $users = $this->user
            ->onlyFollowers()
            ->get();

        return UserResource::collection($users);
    }

    public function following(Request $request)
    {
        $users = $this->user
            ->onlyFollowing()
            ->get();

        return UserResource::collection($users);
    }

    public function friends(Request $request)
    {
        $users = $this->user
            ->friends()
            ->get();

        return UserResource::collection($users);
    }

    public function befriend(Request $request, User $user)
    {
        $user->befriend();

        return response()->json([
            'success'   => true,
            'status'    => $user->friendship
        ]);
    }

    public function unfriend(Request $request, User $user)
    {
        $user->unfriend();

        return response()->json([
            'success'   => true,
            'status'    => $user->friendship
        ]);
    }
}
