<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Like;
use App\Services\ModelMapper;

class LikeController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function toggle(Request $request, $modelName, $modelId)
    {
        $model = ModelMapper::getModel($modelName)->whereId($modelId)->firstOrFail();
        $model->toggleLike();

        return response()->json([
            'success'   => true,
        ]);
    }
}
