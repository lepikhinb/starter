<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attachment;

class AttachmentController extends Controller
{
    private $user;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function upload(Request $request, $modelName)
    {
        $extension = $request->file->getClientOriginalExtension();
        $type = $request->type ?? Attachment::getTypeByExtension($extension);
        $path = $request->file->store($modelName, 'public');
        $size = $request->file->getSize();

        $attachment = Attachment::create([
            'user_id'       => $this->user->id,
            'uuid'          => $request->uuid,
            'model_id'      => 0,
            'model_type'    => $modelName,
            'path'          => $path,
            'extension'     => $extension,
            'name'          => $request->file->getClientOriginalName(),
            'size'          => $size,
            'type'          => $type
        ]);

        return response()->json([
            'success'       => true,
            'attachment_id' => $attachment->id
        ]);
    }

    public function remove(Request $request, $id)
    {
        $attachment = Attachment::findOrFail($id);
        $attachment->delete();

        return response()->json([
            'success'       => true,
        ]);
    }
}
