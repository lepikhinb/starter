<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;

class UserController extends Controller
{
    public function index()
    {
        return User::paginate(10);
        return UserResource::collection(User::paginate(10));
    }

    public function store(CreateRequest $request)
    {
        $user = User::create($request->all(), 201);

        return new UserResource($user);
    }

    public function show(User $user)
    {
        $user->loadMissing('profile');

        return new UserResource($user);
    }

    public function update(UpdateRequest $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['success' => true]);
    }
}
