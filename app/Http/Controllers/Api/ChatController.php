<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat\Conversation;
use App\Models\Chat\Message;
use App\Models\Attachment;
use App\Events\MessagePushed;
use App\Events\MessageRemoved;
use App\Events\MessageUpdated;
use App\Events\NotificationPushed;

class ChatController extends Controller
{
    private $user;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function getConversations()
    {
        $conversations = $this->user
            ->conversations()
            ->with('lastMessage.user')
            ->whereHas('messages')
            ->join('messages', function ($query) {
                $query->on('messages.conversation_id', '=', 'conversations.id')
                    ->on('messages.id', '=', \DB::raw('(select max(id) from messages where messages.conversation_id = conversations.id)'));
            })
            ->groupBy('messages.conversation_id')
            ->orderBy('messages.created_at', 'desc')
            ->select('conversations.*')
            ->get();

        return response()->json($conversations);
    }

    public function show(Request $request, $conversation_id)
    {
        $conversation = Conversation::findOrFail($conversation_id);

        $messages = $conversation->messages()->with('user', 'attachments')->oldest('id')->get();

        $this->user->messages()->syncWithoutDetaching($messages->pluck('id')->toArray());

        return $messages;
    }

    public function sendMessage(Request $request, Conversation $conversation)
    {
        $message = $conversation->messages()->create([
            'user_id'   => $this->user->id,
            'body'      => $request->body
        ]);

        Attachment::handleTemp($message, $request->all());
        $message->load('attachments', 'user');

        broadcast(new MessagePushed($conversation->id, $message))->toOthers();
        broadcast(new NotificationPushed($conversation->users()->where('id', '<>', $this->user->id)->first()->id, [
            'type'      => 'message', 
            'message'   => $message,
        ]));

        return $message;
    }

    public function removeMessage(Request $request, Conversation $conversation, Message $message)
    {
        broadcast(new MessageRemoved($conversation->id, $message))->toOthers();
        $message->delete();

        return $message;
    }

    public function updateMessage(Request $request, Conversation $conversation, Message $message)
    {
        $message->update(['body' => $request->body]);
        broadcast(new MessageUpdated($conversation->id, $message))->toOthers();

        return $message;
    }

    public function start(Request $request, $userId)
    {
        $conversation = $this->user->conversations()->whereHas('users', function ($query) use ($userId) {
            $query->where('id', $userId);
        })->first();

        if(! $conversation)
        {
            $conversation = $this->user
                ->conversations()
                ->create([
                    'user_id' => $this->user->id
                ]);

            $conversation->users()->syncWithoutDetaching($userId);
        }

        return $conversation;
    }

}
