<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Like;
use App\Services\ModelMapper;

class CommentController extends Controller
{
    private $user;
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
    }

    public function store(Request $request, $modelName, $modelId)
    {        
        $model = ModelMapper::getModel($modelName)->whereId($modelId)->firstOrFail();
        $comment = $model->comments()->create([
            'user_id'   => $this->user->id,
            'body'      => $request->body,
        ]);

        $comment->load('user');

        return $comment;
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update([
            'body'  => $request->body,
        ]);

        return $comment;
    }

    public function destroy(Request $request, Comment $comment)
    {
        $comment->delete();

        return response()->json([
            'success'   => true,
        ]);
    }
}
