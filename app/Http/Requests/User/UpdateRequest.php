<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Transform the error messages into JSON
     *
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'string|max:255',
            'last_name'     => 'string|max:255',
            'phone'         => 'string|max:255|unique:users,id,' . $this->id,
            'image'         => 'string|max:255',
            'email'         => 'string|email|max:255|unique:users,id,' . $this->id,
            'password'      => 'nullable|string|min:6',
        ];
    }
}
