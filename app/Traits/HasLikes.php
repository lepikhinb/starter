<?php

namespace App\Traits;

use App\Models\Like;

trait HasLikes
{
    /** relationships **/

    public function likes()
    {
        return $this->morphMany(Like::class, 'model');
    }

    /** functions **/

    public function like()
    {
        $this->likes()->create([
            'user_id'   => auth()->user()->id
        ]);
    }

    public function unlike()
    {
        $this->likes()->where('user_id', auth()->user()->id)->delete();
    }

    public function toggleLike()
    {
        if($this->likes()->where('user_id', auth()->user()->id)->exists())
        {
            $this->unlike();
        }
        else
        {
            $this->like();
        }
    }
    
    /** accessors **/

    public function getIsLikedAttribute()
    {
        return $this->likes()->where('user_id', auth()->user()->id)->exists();
    }
}
