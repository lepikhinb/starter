<?php

namespace App\Traits;

use App\Models\User;

trait Friendship {

    /** relationships **/

    public function followers()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'follower_id');
    }

    public function onlyFollowers()
    {
        return $this->followers()->whereDoesntHave('followers', function ($query) {
            $query->where('follower_id', $this->id);
        });
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'friends', 'follower_id', 'user_id');
    }

    public function onlyFollowing()
    {
        return $this->following()->whereDoesntHave('following', function ($query) {
            $query->where('user_id', $this->id);
        });
    }

    public function friends()
    {
        return $this->following()->whereHas('following', function ($query) {
            $query->where('user_id', $this->id);
        });
    }

    /** accessors **/

    public function getFriendshipAttribute()
    {
        $me = auth()->user();

        if(! $me)
        {
            return User::STATUS_NONE;
        }

        $following = $me->followers->contains($this->id);
        $follower = $me->following->contains($this->id);

        if($following && $follower)
        {
            return User::STATUS_FRIENDS;
        }
        else if($following)
        {
            return User::STATUS_FOLLOWER;
        }
        else if($follower)
        {
            return User::STATUS_FOLLOWING;
        }
        else
        {
            return User::STATUS_NONE;
        }
    }

    /** functions **/

    public function befriend($user = null)
    {
        if(! $user)
        {
            $user = auth()->user();
        }

        $user->following()->syncWithoutDetaching($this->id);
    }

    public function unfriend($user = null)
    {
        if(! $user)
        {
            $user = auth()->user();
        }

        $user->following()->detach($this->id);
    }

}