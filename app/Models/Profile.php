<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /** attributes **/

    protected $guarded = ['id'];

    /** relationships **/

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
