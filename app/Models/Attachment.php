<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasComments;
use App\Traits\HasLikes;

class Attachment extends Model
{
    use HasComments;
    use HasLikes;

    /** attributes **/

    protected $guarded = ['id'];

    protected $appends = [
        'link',
        'is_processed',
        'thumbnail',
    ];

    protected $casts = [
        'processed_at',
    ];

    /** constants **/

    const TYPE_PHOTO    = 'photo';
    const TYPE_VIDEO    = 'video';
    const TYPE_AUDIO    = 'audio';
    const TYPE_OTHER    = 'other';

    public static $types = [
        self::TYPE_PHOTO => [
            'extensions'    => [
                'jpg',
                'jpeg',
                'png',
            ],
        ],
        self::TYPE_VIDEO => [
            'extensions'    => [
                'mp4',
            ],
        ],
        self::TYPE_AUDIO => [
            'extensions'    => [
                'mp3',
            ],
        ],
    ];

    /** relationships **/

    public function model()
    {
        return $this->morphsTo();
    }

    /** accessors **/

    public function getLinkAttribute()
    {
        if(strpos($this->path, 'http://') !== false || strpos($this->path, 'https://') !== false)
        {
            return $this->path;
        }

        return $this->path ? asset('storage/' . $this->path) : null;
    }

    /** scopes **/

    public function scopePhoto($query)
    {
        $query->where('type', self::TYPE_PHOTO);
    }

    public function scopeVideo($query)
    {
        $query->where('type', self::TYPE_VIDEO);
    }

    public function scopeAudio($query)
    {
        $query->where('type', self::TYPE_AUDIO);
    }

    public function scopeOther($query)
    {
        $query->where('type', self::TYPE_OTHER);
    }

    /** static functions **/

    public static function getTypeByExtension($extension)
    {
        $types = self::$types;

        foreach($types as $type => $data)
        {
            foreach($data['extensions'] as $_extension)
            {
                if($_extension === $extension)
                {
                    return $type;
                }
            }
        }

        return self::TYPE_OTHER;
    }

    public static function handleTemp($model, $params)
    {
        if(isset($params['uuid']))
        {
            $uuid = $params['uuid'];

            self::whereUuid($uuid)->whereModelId(0)->update([
                'model_id'  => $model->id,
            ]);
        }
    }

}
