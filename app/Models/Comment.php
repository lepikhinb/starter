<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasLikes;
use App\Traits\HasAttachments;

class Comment extends Model
{
    use HasLikes;
    use HasAttachments;

    /** attributes **/

    protected $guarded = ['id'];

    protected $appends = [
        'is_liked',
        'is_disliked',
        'likes_count',
        'dislikes_count',
    ];

    /** global scopes **/

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->oldest('id');
        });
    }

    /** relationships **/

    public function model()
    {
        return $this->morphsTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
