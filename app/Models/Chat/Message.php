<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Traits\HasAttachments;

class Message extends Model
{
    use HasAttachments;

    /** attributes **/

    protected $guarded = ['id'];

    /** relationships **/

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
