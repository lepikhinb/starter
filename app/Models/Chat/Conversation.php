<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Conversation extends Model
{
    /** attributes **/

    protected $guarded = ['id'];

    protected $with = [
        'users',
    ];

    protected $appends = [
        'display',
        'thumbnail',
        'unread_messages_count',
    ];

    /** relationships **/

    public function model()
    {
        return $this->morphTo();
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->latest()->withDefault();
    }

    /** accessors **/

    public function getUnreadMessagesCountAttribute()
    {
        return $this->messages()->where('user_id', '<>', auth()->user()->id)
            ->whereDoesntHave('users', function ($query) {
                $query->where('id', auth()->user()->id);
            })->count();
    }

    public function getThumbnailAttribute()
    {
        return $this->users->where('id', '<>', auth()->user()->id)->first()->image;
    }

    public function getDisplayAttribute()
    {
        if(isset($this->name))
        {
            return $this->name;
        }

        elseif($this->users->count() >= 2)
        {
            return $this->users->where('id', '<>', auth()->user()->id)->first()->full_name;
        }

        else
        {
            return '–';
        }
    }
}
