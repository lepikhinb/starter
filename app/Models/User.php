<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Chat\Message;
use App\Models\Chat\Conversation;
use App\Traits\HasAttachments;
use App\Traits\HasLikes;
use App\Traits\Friendship;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasAttachments;
    use HasLikes;
    use Friendship;

    /** attributes **/

    protected $protected = ['id'];
    
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'full_name',
        'image_url',
        'friendship',
        'unread_notifications_count',
        'unread_messages_count',
        'is_me',
    ];

    /** constants **/

    public const STATUS_FOLLOWING  = 'following';
    public const STATUS_FOLLOWER   = 'follower';
    public const STATUS_FRIENDS    = 'friends';
    public const STATUS_NONE       = 'none';

    /** jwt **/

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /** relationships **/

    public function profile()
    {
        return $this->hasOne(Profile::class)->withDefault();
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversation::class);
    }

    public function ownConversations()
    {
        return $this->hasMany(Conversation::class);
    }

    public function messages()
    {
        return $this->belongsToMany(Message::class);
    }

    /** accessors **/

    public function getIsMeAttribute()
    {
        return $this->id === auth()->user()->id;
    }

    public function getImageUrlAttribute()
    {
        return $this->image ? asset('storage/' . $this->image) : null;
    }

    public function getFullNameAttribute()
    {
        if(isset($this->first_name) && isset($this->last_name))
        {
            return $this->first_name . ' ' . $this->last_name;
        }
        else
        {
            return isset($this->first_name) ? $this->first_name : $this->last_name;
        }
    }

    public function getUnreadMessagesCountAttribute()
    {
        $conversationsIds = $this->conversations()->pluck('id')->toArray();

        return Message::where('user_id', '<>', $this->id)
            ->whereIn('conversation_id', $conversationsIds)
            ->whereDoesntHave('users', function ($query) {
                $query->where('id', $this->id);
            })
            ->groupBy('conversation_id')
            ->count();
    }

    public function getUnreadNotificationsCountAttribute()
    {
        return $this->unreadNotifications()->count();
    }

    /** mutators **/

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = mb_convert_case($value, MB_CASE_TITLE, 'UTF-8');
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = mb_convert_case($value, MB_CASE_TITLE, 'UTF-8');
    }
}
