<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Relations\Relation;

class ModelMapper 
{
    private static $models;

    public function __construct()
    {
        self::$models = Relation::morphMap();
    }

    public static function getModel($name)
    {
        self::$models = Relation::morphMap();
        return new self::$models[$name]();
    }

    public static function getModelName($name)
    {
        self::$models = Relation::morphMap();
        return self::$models[$name];
    }

}
